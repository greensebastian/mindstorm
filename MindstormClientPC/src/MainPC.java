public class MainPC {

	public static void main(String[] args) {
		ServerSocketManager socMan = new ServerSocketManager();
		socMan.start();
		
		PCGUI gui = new PCGUI(socMan);

		try {
			socMan.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
