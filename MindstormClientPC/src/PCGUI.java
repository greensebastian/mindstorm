import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.*;

public class PCGUI {
	private JFrame mainFrame;
	private JTextArea textInput, textOutput;
	private JButton btn;
	
	public PCGUI(ServerSocketManager socMan){
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container contentPane = mainFrame.getContentPane();
		
		textInput = new JTextArea();
		textOutput = new JTextArea();
		textOutput.setEditable(false);
		
		btn = new JButton("Send to EV3");
		btn.addActionListener((event) -> {
			String msg = textInput.getText();
			if(!msg.equals("")){
				socMan.putMessage(msg);
			}
			textOutput.setText("EV3: " + socMan.doGetMessage());
		});
		
		contentPane.add(textOutput, BorderLayout.NORTH);
		contentPane.add(textInput, BorderLayout.CENTER);
		contentPane.add(btn, BorderLayout.SOUTH);
		
		mainFrame.pack();
		mainFrame.setVisible(true);
	}
}
