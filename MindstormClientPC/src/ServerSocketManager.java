import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class ServerSocketManager extends Thread {
	private ServerSocket server;
	private Socket client;

	private PrintWriter out;
	private BufferedReader in;
	private LinkedList<String> inputBuffer = new LinkedList<String>();
	private Semaphore countSem = new Semaphore(0, true);

	private static final int PORT = 4444; // Server port

	public static final String END_STRING = "end_of_stream";

	public ServerSocketManager() {
		connect();
	}

	private void connect() {
		try {
			/*
			 * Initialize socket and wait for client to connect. Create and
			 * connect reader/writer to connected socket
			 */
			System.out.println("Creating socket");
			server = new ServerSocket(PORT);
			System.out.println("Waiting for connection...");
			client = server.accept();
			System.out.println("Connected to " + client.getInetAddress().getHostAddress());

			// Create streams and set appropriate charset
			out = new PrintWriter(new OutputStreamWriter(client.getOutputStream(), Charset.forName("UTF-8")), true);
			in = new BufferedReader(new InputStreamReader(client.getInputStream(), Charset.forName("UTF-8")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				// Blocking read from socket
				String read = in.readLine();
				// Close thread if connection terminated by client
				if (read == null || read.indexOf(END_STRING) >= 0) {
					this.join();
					continue;
				}
				// Put message in buffer
				synchronized (inputBuffer) {
					inputBuffer.add(read);
				}
				countSem.release();
			}

			/*
			 * Thread end of life
			 */
			System.out.println("ServerSocketManager stopped, closing connection");
			client.close();
			server.close();

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends message through socket.
	 * 
	 * @param msg
	 *            message to be sent
	 */
	public synchronized void putMessage(String msg) {
		out.println(msg);
	}

	/**
	 * Attempts to retrieve oldest message from buffer.
	 * 
	 * @return oldest non-read message available, null if no messages
	 */
	public synchronized String tryGetMessage() {
		String msg = inputBuffer.pollFirst();
		if (msg != null) {
			try {
				countSem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return msg;
	}

	/**
	 * Retrieves oldest message from buffer. Blocks until message is available.
	 * 
	 * @return oldest non-read message available
	 */
	public synchronized String doGetMessage() {
		String msg = null;
		try {
			countSem.acquire();
			msg = inputBuffer.pollFirst();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * Checks if there are messages to be read.
	 * 
	 * @return true if non-read message available
	 */
	public synchronized boolean hasMessage() {
		return !inputBuffer.isEmpty();
	}
}
