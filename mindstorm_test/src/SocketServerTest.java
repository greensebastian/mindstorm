import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServerTest {
	public static void main(String[] args) throws IOException {
		System.out.println("Creating socket");
		ServerSocket serverSocket = new ServerSocket(4444);
		System.out.println("Waiting for connection...");
		Socket clientSocket = serverSocket.accept();
		System.out.println("Connected to: " + clientSocket.getInetAddress().getHostAddress());
		
		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		String inputLine, outputLine;
		outputLine = "Hello World!";
		out.println(outputLine);
		
		System.out.println("Closing connection");
		serverSocket.close();
	}
}
