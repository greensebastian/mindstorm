import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class BTNetTest {

	public static void main(String[] args) {
		URL ev3URL = null;
		BufferedReader in = null;
		URLConnection conn = null;
		
		try {
			ev3URL = new URL("http://10.0.1.1");
			conn = ev3URL.openConnection();
			conn.connect();
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			in.close();
		} catch (MalformedURLException e) {
			System.out.println("Error creating URL.");
			e.printStackTrace();
		} catch (IOException e){
			System.out.println("Error opening connection.");
			e.printStackTrace();
		}
	}
}
