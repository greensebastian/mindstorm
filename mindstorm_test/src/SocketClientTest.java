import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import lejos.hardware.lcd.LCD;
import lejos.utility.Delay;

public class SocketClientTest {

	public static void main(String[] args) throws UnknownHostException, IOException {
		String hostName = "10.0.1.3";
		int portNumber = 4444;
		Socket socket = new Socket(hostName, portNumber);
		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		String fromServer;

		while ((fromServer = in.readLine()) != null) {
			if (fromServer.indexOf("end") >= 0){
				LCD.drawString("Connection terminating...", 0, 0);
				Delay.msDelay(2000);
				break;
			}
			else {
				LCD.clear();
				LCD.drawString(fromServer, 0, 0);
			}
		}

		out.close();
		socket.close();
	}

}
