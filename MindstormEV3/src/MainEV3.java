import lejos.hardware.lcd.LCD;

public class MainEV3 {

	public static void main(String[] args) {
		ClientSocketManager socMan = new ClientSocketManager();
		socMan.start();

		String msg = "";
		int i = 0;

		while (socMan.isAlive()) {
			msg = socMan.doGetMessage();
			if (msg == null) continue;
			socMan.putMessage(msg);
			LCD.drawString(msg, 0, i + 2);
			i++;
		}

		try {
			socMan.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
